from subprocess import Popen, PIPE, STDOUT
from select import select
from time import sleep
from os.path import join as join_path
from threading import Thread
from Queue import Queue, Empty
import shlex


class Config(object):


    def __init__(self):
        self.min_mem = 256
        self.max_mem = 512
        self.mc_path = "/data/mc/"
        self.mc_jar = "minecraft_server.jar"
        self.java_flags = ""
        self.jar_flags = "nogui"


    def from_dict(self, configs):
        for key in configs:
            setattr(self, key, configs[key])


    @property
    def command(self):
	return "java -Xmx%iM -Xms%iM %s -jar %s %s" % (
	    self.max_mem, self.min_mem, self.java_flags, join_path(self.mc_path, self.mc_jar), self.jar_flags
	    )


    @property
    def args(self):
	return shlex.split(self.command)


class MCServer(object):

    def __init__(self):
        self.config = Config()
        self.server_process = None
        self.read_queue = Queue()
        self.write_queue = Queue()
        self.last_command = None
 

    @property
    def messages(self):
        return self.read_queue.qsize()


    @property
    def running(self):
        if self.server_process <> None \
                and self.server_process.poll() == None:
            return True
        else:
            return False



    def __write_loop(self):
        while self.running:
            sel = select([], [self.server_process.stdin.fileno()],[])
            if self.running:
                try:
                    msg = self.write_queue.get(False)
                except Empty:
                    msg = None
                while msg is not None:
                    for fh in sel[1]:
                        if fh == self.server_process.stdin.fileno():
                            self.server_process.stdin.write(msg)
                            self.server_process.stdin.flush()
                            msg = None
            else:
                break
            sleep(0.1)


    def __read_loop(self):
        while self.running:
            if self.running:
                sel = select([self.server_process.stdout.fileno()],[],[])
                for fh in sel[0]:
                    if fh == self.server_process.stdout.fileno():
                        line = self.server_process.stdout.readline()
                        if line <> '':
                            self.read_queue.put(line)
            else:
                self.server_process.stdout.close()
                break


    def start(self):
        # Sample startup command: java -Xmx2048M -Xms2048M -jar minecraft_server.jar nogui
        if not self.running:
            self.server_process = Popen(
                    args=self.config.args, cwd=self.config.mc_path, stdin=PIPE, stdout=PIPE, stderr=STDOUT
                )
            self.reader = Thread(target=self.__read_loop)
            self.reader.start()
            self.writer = Thread(target=self.__write_loop)
            self.writer.start()


    def stop(self):
        if self.running:
            self.write('stop')


    def terminate(self):
        if self.running:
            self.server_process.terminate()


    def kill(self):
        if self.running:
            self.server_process.kill()


    def read(self, blocking=False):
        try:
            message = self.read_queue.get(blocking).rstrip()
            self.read_queue.task_done()
        except Empty:
            message = None
        return message


    def readall(self):
        while not self.read_queue.empty():
            try:
                yield self.read(False).rstrip()
            except Empty:
                yield None


    def write(self, cmd):
        self.write_queue.put(cmd.rstrip() + '\n')
