import tornado.ioloop
import tornado.web
import tornado.httputil
from MinedControl import MCServer
import handlers
import config


mcs = MCServer()
mcs.config.from_dict(config.MinedControlConfig)


application = tornado.web.Application([
        (r"/", handlers.GTFO),
        (r"/mcapi", handlers.MCAPI, dict(mcs= mcs)),
        (r"/favicon.ico", handlers.Favicon),
])


if __name__ == "__main__":
    application.listen(5000)
    tornado.ioloop.IOLoop.instance().start()
