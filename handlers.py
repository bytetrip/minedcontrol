import tornado.ioloop
import tornado.web
import tornado.httputil


class MCAPI(tornado.web.RequestHandler):


    def initialize(self, mcs):
        self.mcs = mcs
        self.quickform = """
            <html>
            <head>
            <title>CHEEZY API CONTROLS</title>
            </head>
            <body>
            <form class="form-signin" method="POST" action="">
            <h3 class="form-signin-heading">Yes?</h3>
            <p>Last command:<em> %s <em></p>
            <input class="form-control" type="text" autocomplete="off" required name="cmd" placeholder="Command"><br />
            <input class="form-control" type="password" required name="password" placeholder="Password"><br />
            <button class="btn btn-lg btn-primary btn-block" type="submit">Send</button>
            </form>
            </body>
            </html>""" %  (self.mcs.last_command)


    def get(self):
        self.add_header('content-type', 'text/html')
        self.write(self.quickform)


    def post(self):
        if self.get_argument('password') == 'test':
            cmd = self.get_argument('cmd')
            if cmd == 'start':
                self.mcs.start()
                self.mcs.last_command = 'start'
            elif cmd == 'terminate':
                self.mcs.terminate()
                self.mcs.last_command = 'terminate'
            elif cmd == 'kill':
                self.mcs.kill()
                self.mcs.last_command = 'kill'
            elif cmd == 'dump':
                output = ''
                for line in self.mcs.readall():
                    output += line + '\n'
                self.write(output)
            else:
                self.mcs.write(cmd)
                self.mcs.last_command = cmd
        else:
            self.set_status(302)
            self.add_header('Location', 'http://google.com')
        self.finish()

class GTFO(tornado.web.RequestHandler):

    def get(self):
        self.set_status(302)
        self.add_header('Location', 'http://google.com')


class Favicon(tornado.web.RequestHandler):
    def get(self):
        with open('favicon.ico', 'rb') as f:
            data = f.read()
            self.write(data)
        self.finish()
